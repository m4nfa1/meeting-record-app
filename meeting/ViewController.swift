//
//  ViewController.swift
//  meeting
//
//  Created by Man Wing Fai on 31/3/2017.
//  Copyright © 2017年 Man Wing Fai. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {

    var timer: Timer!
    var count = 0
    
    @IBOutlet weak var strLabel: UILabel!
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var strRecord: UIButton!
    @IBOutlet weak var pauseBtn: UIButton!
    @IBOutlet weak var clearBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
//        do{
//            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
//            try recordingSession.setActive(true)
//            recordingSession.requestRecordPermission(){
//                [unowned self] allowed in DispatchQueue.main.async {
//                    if allowed{
//                        self.strLabel.text = "GO"
//                    }
//                    else {
//                        //failed to record
//                    }
//                    }
//                }
//        } catch{
//        
//        
//        }
//
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func countTime(){
        count += 1
        NSLog("ok")
        countLabel.text = formatTime(time: count)
    }
    
    func formatTime(time:Int) -> String{
        let hours = Int(time)/3600
        let minutes = Int(time)/60%60
        let seconds = Int(time)%60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds);
        
    }
    
    @IBAction func startRecord(sender: AnyObject){
        timer = Timer.scheduledTimer(timeInterval: 1.0, target:self, selector: #selector(self.countTime),
                                     userInfo:nil,
                                     repeats: true)
        
    }
    @IBAction func pauseRecord(sender: AnyObject){
        
    }
    @IBAction func clearRecord(sender: AnyObject){
        timer.invalidate()
        count = 0
        
    }
    
    
    
}

